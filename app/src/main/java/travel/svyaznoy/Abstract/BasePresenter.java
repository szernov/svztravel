package travel.svyaznoy.Abstract;

public interface BasePresenter<T> {


    /**
     * Bind presenter of the view when resumed. The presenter will perform initialisation here.
     *
     * @param view
     */
    void takeView(T view);

    /**
     * Drop the reference of the view when view was destroyed
     */
    void dropView();
}
