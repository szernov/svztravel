package travel.svyaznoy;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import travel.svyaznoy.di.DaggerAppComponent;

public class SvzApplication extends DaggerApplication {
    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().application(this).build();
    }
}
