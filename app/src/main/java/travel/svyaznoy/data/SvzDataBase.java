package travel.svyaznoy.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import travel.svyaznoy.data.entity.New;
import travel.svyaznoy.data.news.NewsDao;

@Database(entities = {New.class}, version = 1)
public abstract class SvzDataBase extends RoomDatabase {

    public abstract NewsDao newsDao();
}
