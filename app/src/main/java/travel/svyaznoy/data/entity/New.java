package travel.svyaznoy.data.entity;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.TypeConverters;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import travel.svyaznoy.data.news.DateConverter;

@Entity(tableName = "news")
public class New implements Parcelable {

    @PrimaryKey
    @SerializedName("Id")
    public final int id;

    @SerializedName("Title")
    public final String title;

    @SerializedName("Annotation")
    public final String annotation;

    @SerializedName("Text")
    public final String text;

    @SerializedName("NewsDate")
    @TypeConverters(DateConverter.class)
    public final Date newsDate;

    @SerializedName("Type")
    public final int type;

    @SerializedName("ImgUrl")
    public final String imageUrl;

    @SerializedName("OnTop")
    public final boolean onTop;

    @SerializedName("ChangeDate")
    @TypeConverters(DateConverter.class)
    public final Date changeDate;

    @SerializedName("ValidDate")
    @TypeConverters(DateConverter.class)
    public final Date validDate;

    @SerializedName("IsRead")
    public final boolean isRead;

    public String htmlString;

    public New(int id, String title, String annotation, String text,
               Date newsDate, int type, String imageUrl, boolean onTop,
               Date changeDate, Date validDate, boolean isRead) {
        this.id = id;
        this.title = title;
        this.annotation = annotation;
        this.text = text;
        this.newsDate = newsDate;
        this.type = type;
        this.imageUrl = imageUrl;
        this.onTop = onTop;
        this.changeDate = changeDate;
        this.validDate = validDate;
        this.isRead = isRead;
        htmlString = CreateHtmlFromObject();
    }

    protected New(Parcel in) {
        newsDate = (Date) in.readSerializable();
        changeDate = (Date) in.readSerializable();
        validDate = (Date) in.readSerializable();
        id = in.readInt();
        title = in.readString();
        annotation = in.readString();
        text = in.readString();
        type = in.readInt();
        imageUrl = in.readString();
        onTop = in.readByte() != 0;
        isRead = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(annotation);
        dest.writeString(text);
        dest.writeInt(type);
        dest.writeString(imageUrl);
        dest.writeByte((byte) (onTop ? 1 : 0));
        dest.writeByte((byte) (isRead ? 1 : 0));
        dest.writeSerializable(newsDate);
        dest.writeSerializable(changeDate);
        dest.writeSerializable(validDate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<New> CREATOR = new Creator<New>() {
        @Override
        public New createFromParcel(Parcel in) {
            return new New(in);
        }

        @Override
        public New[] newArray(int size) {
            return new New[size];
        }
    };

    private String CreateHtmlFromObject() {
        String result = "<!DOCTYPE html><html><body>";
        result += "<h2 class=\"mp_news_title\">" + title + "</h2>";
        result += "<div class=\"mp_news_date\">" + newsDate + "</div>";
        result += "<div class=\"mp_news_annotation\">" + annotation + "</div>";
        //TODO: если картинки нет - не показываем этот тег!
        result += "<img class=\"mp_news_img\" src=\"" + imageUrl + "\">";
        result += "<div class=\"mp_news_body\">" + text + "</div>";
        result += "\n</body></html>";
        return result;
    }
}
