package travel.svyaznoy.data.news;

import android.arch.persistence.room.TypeConverter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConverter {

    private static DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'T'ZD");

    @TypeConverter
    public static Date toDate(String stringDate){
        Date date = null;
        try {
            date = formatter.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    @TypeConverter
    public static String toStringDate(Date date) {
        return formatter.format(date);
    }
}
