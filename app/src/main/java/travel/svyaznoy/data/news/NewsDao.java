package travel.svyaznoy.data.news;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;
import travel.svyaznoy.data.entity.New;

@Dao
public interface NewsDao {

    @Query("SELECT * FROM news")
    Flowable<List<New>> getNews();


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertNews(List<New> newList);
}
