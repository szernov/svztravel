package travel.svyaznoy.data.news;

import java.util.List;

import io.reactivex.Flowable;
import travel.svyaznoy.data.entity.New;

public interface NewsDataSource {


    Flowable<List<New>> getNews();

    Flowable<New> getNewFromId(int id);

    void refreshNews();

    void saveNews(List<New> newList);
}
