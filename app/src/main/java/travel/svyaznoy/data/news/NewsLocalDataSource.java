package travel.svyaznoy.data.news;


import java.util.List;

import javax.inject.Inject;

import dagger.internal.Preconditions;
import io.reactivex.Flowable;
import io.reactivex.annotations.NonNull;
import travel.svyaznoy.data.entity.New;

public class NewsLocalDataSource implements NewsDataSource {

    private final NewsDao newsDao;

    @Inject
    public NewsLocalDataSource(NewsDao newsDao) {
        this.newsDao = newsDao;
    }


    @Override
    public Flowable<List<New>> getNews() {
        return newsDao.getNews();
    }

    @Override
    public Flowable<New> getNewFromId(int id) {
        return null;
    }

    @Override
    public void refreshNews() {

    }

    @Override
    public void saveNews(@NonNull List<New> newList) {
        Preconditions.checkNotNull(newList);
        newsDao.insertNews(newList);
    }
}
