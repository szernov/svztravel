package travel.svyaznoy.data.news;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import travel.svyaznoy.data.entity.New;
import travel.svyaznoy.data.source.SvzApi;

@Singleton
public class NewsRemoteDataSource implements NewsDataSource {

    SvzApi svzApi;

    String lastChangeDate = "{\"LastChangeDate\":\"\"}";

    @Inject
    public NewsRemoteDataSource(SvzApi svzApi) {
        this.svzApi = svzApi;
    }

    @Override
    public Flowable<List<New>> getNews() {
        return svzApi.getNewsList(lastChangeDate).map(newsResponse -> newsResponse.newsList);
    }

    @Override
    public Flowable<New> getNewFromId(int id) {
        return null;
    }

    @Override
    public void refreshNews() {

    }

    @Override
    public void saveNews(List<New> newList) {

    }
}
