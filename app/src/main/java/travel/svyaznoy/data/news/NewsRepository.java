package travel.svyaznoy.data.news;


import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import travel.svyaznoy.data.entity.New;
import travel.svyaznoy.data.source.Local;
import travel.svyaznoy.data.source.Remote;

@Singleton
public class NewsRepository implements NewsDataSource {

    private final NewsDataSource remoteDataSource;

    private final NewsDataSource localDataSource;

    List<New> cachedNews;

    @Inject
    public NewsRepository(@Remote NewsDataSource remoteDataSource, @Local NewsDataSource localDataSource) {
        this.remoteDataSource = remoteDataSource;
        this.localDataSource = localDataSource;
    }


    @Override
    public Flowable<List<New>> getNews() {
//
//        Flowable<List<New>> flowableRemote = getAndSaveRemoteNews();
//        return remoteDataSource.getNews();

        if(cachedNews == null)
            cachedNews = new ArrayList<>();
        else
            if(!cachedNews.isEmpty())
                return Flowable.fromIterable(cachedNews).toList().toFlowable();
        Flowable<List<New>> flowableRemote = getAndSaveRemoteNews();
        Flowable<List<New>> flowableLocal = getAndCachedLocalNews();
        return flowableRemote
                .onErrorResumeNext(flowableLocal);
    }

    private Flowable<List<New>> getAndSaveRemoteNews(){
        return remoteDataSource.getNews()
                    .doOnNext(newList -> {
                        localDataSource.saveNews(newList);
                        cachedNews = newList;
                    });
    }

    public Flowable<List<New>> getAndCachedLocalNews() {
        return localDataSource.getNews()
                            .doOnNext( newList -> cachedNews = newList);
    }

    @Override
    public Flowable<New> getNewFromId(int id) {
        return null;
    }

    @Override
    public void refreshNews() {

    }

    @Override
    public void saveNews(List<New> newList) {

    }
}
