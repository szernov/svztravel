package travel.svyaznoy.data.news;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import travel.svyaznoy.data.source.Local;
import travel.svyaznoy.data.source.Remote;

@Module
abstract public class NewsRepositoryModule {


    @Singleton
    @Binds
    @Local
    abstract NewsDataSource provideLocalNewsDataSource(NewsLocalDataSource dataSource);


    @Singleton
    @Binds
    @Remote
    abstract NewsDataSource provideRemoteNewsDataSource(NewsRemoteDataSource dataSource);

}
