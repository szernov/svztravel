package travel.svyaznoy.data.responsemodels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import travel.svyaznoy.data.entity.BaseResponse;
import travel.svyaznoy.data.entity.New;

public class NewsResponse extends BaseResponse {

    @SerializedName("News")
    public List<New> newsList;
}
