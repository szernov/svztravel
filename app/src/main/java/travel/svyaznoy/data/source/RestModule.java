package travel.svyaznoy.data.source;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.PUT;

@Module
@Singleton
public class RestModule {

    @Provides
    @Singleton
    public SvzApi provideSvzApi(){
        return provideRetrofitBuilder().build().create(SvzApi.class);
    }


    @Provides
    Retrofit.Builder provideRetrofitBuilder(){
        return new Retrofit.Builder()
                .baseUrl(SvzApi.TEST_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient.Builder()
                                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                                .build());
    }
}
