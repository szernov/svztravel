package travel.svyaznoy.data.source;

import io.reactivex.Flowable;
import io.reactivex.annotations.NonNull;
import retrofit2.http.Body;
import retrofit2.http.POST;
import travel.svyaznoy.data.responsemodels.NewsResponse;

public interface SvzApi {


    String BASE_URL = "https://www.svyaznoy.travel/m_api/v6/";
    String TEST_URL = "https://h21.svyaznoy.travel/m_api/v6/";


    @POST("getnews")
    Flowable<NewsResponse> getNewsList(@NonNull @Body String LastChangeDate);

}
