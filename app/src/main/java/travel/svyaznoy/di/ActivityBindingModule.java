package travel.svyaznoy.di;


import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import travel.svyaznoy.ui.MainActivity;
import travel.svyaznoy.ui.newDetail.NewDetailModule;
import travel.svyaznoy.ui.news.NewsModule;
import travel.svyaznoy.ui.splash.SplashModule;

@Module
public abstract class ActivityBindingModule {
    @ContributesAndroidInjector(modules = {SplashModule.class, NewsModule.class, NewDetailModule.class})
    abstract MainActivity mainActivity();
}
