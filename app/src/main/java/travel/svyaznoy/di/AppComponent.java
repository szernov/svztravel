package travel.svyaznoy.di;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import travel.svyaznoy.SvzApplication;
import travel.svyaznoy.data.news.NewsRepositoryModule;
import travel.svyaznoy.data.source.RestModule;


@Singleton
@Component(modules = {ApplicationModule.class,
        AndroidSupportInjectionModule.class,
        ActivityBindingModule.class,
        RestModule.class,
        SchedulerModule.class,
        NewsRepositoryModule.class
})
public interface AppComponent extends AndroidInjector<SvzApplication> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }
}
