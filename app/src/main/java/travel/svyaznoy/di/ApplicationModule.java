package travel.svyaznoy.di;


import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import travel.svyaznoy.SvzApplication;
import travel.svyaznoy.data.SvzDataBase;
import travel.svyaznoy.data.news.NewsDao;

@Module
public abstract class ApplicationModule {
    @Binds
    abstract Context bindContext(Application application);


    @Singleton @Provides
     static SvzDataBase provideDb(Application application){
        return Room.databaseBuilder(application,
                SvzDataBase.class,
                "svzDb.db")
                .build();
    }

    @Singleton @Provides
    static NewsDao newsDao(SvzDataBase dataBase){
        return dataBase.newsDao();
    }


}
