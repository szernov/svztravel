package travel.svyaznoy.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import travel.svyaznoy.utils.BaseSchedulerProvider;
import travel.svyaznoy.utils.SchedulerProvider;

@Module
public class SchedulerModule {

    @Singleton
    @Provides
    static BaseSchedulerProvider provideScheduler() {
        return SchedulerProvider.getInstance();
    }

}
