package travel.svyaznoy.ui.newDetail;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import travel.svyaznoy.R;
import travel.svyaznoy.data.entity.New;

public class NewDetailFragment extends DaggerFragment {


    @BindView(R.id.bar)
    ProgressBar progressBar;

    @BindView(R.id.webView)
    WebView webView;

    String htmlString;
    New newInfo;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_new_detail,container,false);
        ButterKnife.bind(this,rootView);
//        htmlString = getArguments().getString("htmlString");
        newInfo = getArguments().getParcelable("new_info");
        LoadNewsToWebView();
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public static NewDetailFragment newInstance(New newInfo) {
        Bundle args = new Bundle();
        args.putParcelable("new_info", newInfo);
        NewDetailFragment fragment = new NewDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private void LoadNewsToWebView() {
        progressBar.setProgress(0);
        WebChromeClient client = new WebChromeClient() {
            @Override
            public void onProgressChanged(android.webkit.WebView view, int newProgress) {
                progressBar.setProgress(newProgress);
            }
        };
        webView.setWebChromeClient(client);
        webView.setBackgroundColor(getResources().getColor(R.color.new_background_color));
        webView.getSettings().setJavaScriptEnabled(true);
        String html = CreateHtmlFromObject();
        webView.loadDataWithBaseURL("", html, "text/html", "UTF-8", "");
    }

    private String CreateHtmlFromObject() {
        Log.e("html","Start processing html");
        String result = "<!DOCTYPE html><html><body>";
        result += "<h2 class=\"mp_news_title\">" + newInfo.title + "</h2>";
        result += "<div class=\"mp_news_date\">" + newInfo.newsDate + "</div>";
        result += "<div class=\"mp_news_annotation\">" + newInfo.annotation + "</div>";
        //TODO: если картинки нет - не показываем этот тег!
        result += "<img class=\"mp_news_img\" src=\"" + newInfo.imageUrl + "\">";
        result += "<div class=\"mp_news_body\">" + newInfo.text + "</div>";
        result += "\n</body></html>";
        Log.e("html","Finish processing html");
        return result;
    }
}
