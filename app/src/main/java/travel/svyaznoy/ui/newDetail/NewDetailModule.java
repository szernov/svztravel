package travel.svyaznoy.ui.newDetail;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class NewDetailModule {

    @ContributesAndroidInjector
    abstract NewDetailFragment newsDetailFragment();
}
