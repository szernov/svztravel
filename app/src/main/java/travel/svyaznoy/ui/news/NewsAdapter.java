package travel.svyaznoy.ui.news;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import travel.svyaznoy.R;
import travel.svyaznoy.data.entity.New;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewVH> {

    private List<New> news;
    public static DateFormat formatter = new SimpleDateFormat("dd.MM.YYYY");
    private View.OnClickListener onClickListener;


    public NewsAdapter(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
        this.news = new ArrayList<>();
    }

    @NonNull
    @Override
    public NewVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_new,parent,false);
        return new NewVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewVH holder, int position) {
        holder.title.setText(news.get(position).title);
        holder.date.setText(formatter.format(news.get(position).newsDate));
//        holder.itemView.setTag(news.get(position).htmlString);
        holder.itemView.setTag(news.get(position));
        holder.itemView.setOnClickListener(onClickListener);
    }

    @Override
    public int getItemCount() {
        return news.size();
    }


    public void addNews(List<New> items) {
        news.clear();
        news.addAll(items);
        notifyDataSetChanged();
    }

    class NewVH extends RecyclerView.ViewHolder{
        TextView title, date;

        public NewVH(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.new_title);
            date = itemView.findViewById(R.id.new_date);
        }
    }
}
