package travel.svyaznoy.ui.news;

import java.util.List;

import travel.svyaznoy.Abstract.BasePresenter;
import travel.svyaznoy.Abstract.BaseView;
import travel.svyaznoy.data.entity.New;

public interface NewsContract {


    interface View extends BaseView<Presenter>{

        void showProgressBar();

        void hideProgressBar();

        void showLoadingNewsError();

        void addNews(List<New> news);
        }

    interface Presenter extends BasePresenter<View>{

        void loadNews(boolean isRefresh);

        @Override
        void takeView(View view);

        @Override
        void dropView();
    }
}
