package travel.svyaznoy.ui.news;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerFragment;
import travel.svyaznoy.R;
import travel.svyaznoy.data.entity.New;
import travel.svyaznoy.utils.NavigationController;

public class NewsFragment extends DaggerFragment implements NewsContract.View, SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.news_list)
    RecyclerView newsList;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.retry)
    Button retryButton;

    @OnClick(R.id.retry)
    public void OnRetryButtonClick(){
        retryButton.setVisibility(View.GONE);
        mPresenter.loadNews(false);
    }

    @Inject
    NewsContract.Presenter mPresenter;

    private NewsAdapter adapter;

    @Inject
    NavigationController navigationController;



    private View.OnClickListener onClickListener = v -> navigationController.navigateToNewsDetails((New)v.getTag());

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_news,container,false);
        ButterKnife.bind(this,rootView);
        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new NewsAdapter(onClickListener);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        newsList.setAdapter(adapter);
        newsList.addItemDecoration(new DividerItemDecoration(newsList.getContext(),DividerItemDecoration.HORIZONTAL));
        swipeRefreshLayout.setOnRefreshListener(this);
    }


    @Override
    public void onResume() {
        super.onResume();
        mPresenter.takeView(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.dropView();
    }

    @Override
    public void onDestroyView() {
        if(newsList!=null)
            newsList.setAdapter(null);
        super.onDestroyView();
    }

    public static NewsFragment newInstance() {
        
        Bundle args = new Bundle();
        NewsFragment fragment = new NewsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showLoadingNewsError() {
        progressBar.setVisibility(View.GONE);
        retryButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void addNews(List<New> news) {
        adapter.addNews(news);
    }

    @Override
    public void onRefresh() {
        mPresenter.loadNews(true);
        swipeRefreshLayout.setRefreshing(true);
    }
}
