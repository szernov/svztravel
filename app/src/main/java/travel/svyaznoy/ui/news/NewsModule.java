package travel.svyaznoy.ui.news;


import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
import travel.svyaznoy.data.SvzDataBase;
import travel.svyaznoy.data.news.NewsDao;
import travel.svyaznoy.ui.splash.SplashContract;
import travel.svyaznoy.ui.splash.SplashPresenter;

@Module
public abstract class NewsModule {


    @ContributesAndroidInjector
    abstract NewsFragment newsFragment();

    @Binds
    abstract NewsContract.Presenter newsPresenter(NewsPresenter presenter);

//    @Singleton @Provides
//    static NewsDao newsDao(SvzDataBase dataBase){
//        return dataBase.newsDao();
//    }


}
