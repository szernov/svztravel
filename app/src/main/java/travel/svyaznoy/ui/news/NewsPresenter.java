package travel.svyaznoy.ui.news;

import android.support.annotation.NonNull;

import javax.annotation.Nullable;
import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import travel.svyaznoy.data.news.NewsRepository;
import travel.svyaznoy.utils.BaseSchedulerProvider;

public class NewsPresenter implements NewsContract.Presenter {


    @Nullable
    NewsContract.View newsFragment;

    @Inject @NonNull
    BaseSchedulerProvider mSchedulerProvider;

    @NonNull
    private final NewsRepository repository;

    @NonNull
    private CompositeDisposable mCompositeDisposable;


    @Inject
    public NewsPresenter(NewsRepository repository) {
        this.repository = repository;
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public void loadNews(boolean isRefresh) {
        if(newsFragment!=null && !isRefresh)
            newsFragment.showProgressBar();

        Disposable disposable =
        repository.getNews()
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(news -> {
                    newsFragment.addNews(news);
                    newsFragment.hideProgressBar();
                    },throwable -> newsFragment.showLoadingNewsError());

        mCompositeDisposable.add(disposable);
    }

    @Override
    public void takeView(NewsContract.View view) {
        newsFragment = view;
        loadNews(false);
    }

    @Override
    public void dropView() {

    }
}
