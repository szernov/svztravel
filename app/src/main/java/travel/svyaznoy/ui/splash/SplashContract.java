package travel.svyaznoy.ui.splash;

import travel.svyaznoy.Abstract.BasePresenter;
import travel.svyaznoy.Abstract.BaseView;

public interface SplashContract {

    interface View extends BaseView<Presenter>{

        void setLoadingIndicator();

        void configurationLoaded();

        void showLoadingConfigurationError();

    }


    interface Presenter extends BasePresenter<View>{

        void loadConfiguration();

        void takeView(View view);

        void dropView();
    }
}
