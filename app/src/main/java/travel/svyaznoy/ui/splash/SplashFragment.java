package travel.svyaznoy.ui.splash;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import travel.svyaznoy.Abstract.BaseView;
import travel.svyaznoy.R;

public class SplashFragment extends DaggerFragment implements SplashContract.View {

    @Inject
    SplashContract.Presenter mPresenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_splash,container,false);
        ButterKnife.bind(this,rootView);
        return rootView;
    }


    public static SplashFragment newInstance() {
        Bundle args = new Bundle();
        SplashFragment fragment = new SplashFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void setLoadingIndicator() {

    }

    @Override
    public void configurationLoaded() {

    }

    @Override
    public void showLoadingConfigurationError() {

    }
}
