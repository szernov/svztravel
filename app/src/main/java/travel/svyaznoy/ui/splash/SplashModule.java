package travel.svyaznoy.ui.splash;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class SplashModule {

    @ContributesAndroidInjector
    abstract SplashFragment splashFragment();

    @Binds
    abstract SplashContract.Presenter splashPresenter(SplashPresenter presenter);
}
