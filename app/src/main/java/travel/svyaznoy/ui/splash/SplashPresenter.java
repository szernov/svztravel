package travel.svyaznoy.ui.splash;

import javax.annotation.Nullable;
import javax.inject.Inject;

public class SplashPresenter implements SplashContract.Presenter {



    @Nullable
    private SplashContract.View mSplashView;

    @Inject
    public SplashPresenter() {
    }

    @Override
    public void loadConfiguration() {

    }

    @Override
    public void takeView(SplashContract.View view) {
        this.mSplashView = view;
        loadConfiguration();
    }

    @Override
    public void dropView() {
        mSplashView = null;

    }
}
