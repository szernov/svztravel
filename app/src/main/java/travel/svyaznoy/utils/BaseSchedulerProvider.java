package travel.svyaznoy.utils;

import io.reactivex.Scheduler;
import io.reactivex.annotations.NonNull;

public interface BaseSchedulerProvider {


    @NonNull
    Scheduler io();

    @NonNull
    Scheduler ui();
}
