package travel.svyaznoy.utils;

import android.support.v4.app.FragmentManager;

import javax.inject.Inject;

import travel.svyaznoy.R;
import travel.svyaznoy.data.entity.New;
import travel.svyaznoy.ui.MainActivity;
import travel.svyaznoy.ui.newDetail.NewDetailFragment;
import travel.svyaznoy.ui.news.NewsFragment;
import travel.svyaznoy.ui.splash.SplashFragment;

public class NavigationController {


    private final int containerId;
    private final FragmentManager fragmentManager;

    @Inject
    public NavigationController(MainActivity mainActivity) {
        this.containerId = R.id.container_layout;
        this.fragmentManager = mainActivity.getSupportFragmentManager();
    }

    public void navigateToSplash(){
        SplashFragment fragment = SplashFragment.newInstance();
        fragmentManager.beginTransaction()
                .add(containerId,fragment)
                .commit();
    }

    public void navigateToNews(){
        NewsFragment fragment = NewsFragment.newInstance();
        fragmentManager.beginTransaction()
                .add(containerId,fragment)
                .addToBackStack(null)
                .commit();
    }

    public void navigateToNewsDetails(New newInfo){
        NewDetailFragment fragment = NewDetailFragment.newInstance(newInfo);
        fragmentManager.beginTransaction()
                .add(containerId,fragment)
                .addToBackStack(null)
                .commit();
    }
}
